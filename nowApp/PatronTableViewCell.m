//
//  PatronTableViewCell.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "PatronTableViewCell.h"

@implementation PatronTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
