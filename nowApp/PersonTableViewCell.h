//
//  PersonTableViewCell.h
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface PersonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *bioLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalWebsiteLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIStackView *itemsStack;
@property (weak, nonatomic) IBOutlet UIView *moreDotsView;
@property BOOL isExpanded;

-(void) configureCell: (User *)data;
-(void) toggleCellExpansion;
-(void) addItems;

@end
