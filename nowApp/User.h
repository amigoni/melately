//
//  User.h
//  nowApp
//
//  Created by Leonardo Amigoni on 2/10/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "JSONModel.h"
#import "ProfileInfo.h"
#import "Follower.h"
#import "Following.h"
#import "Item.h"

@interface User : JSONModel

@property (strong, nonatomic) ProfileInfo* profileInfo;
@property (strong, nonatomic) NSMutableArray<NSString*><Optional>* following;
@property (strong, nonatomic) NSMutableArray<NSString*><Optional>* followers;
@property (strong, nonatomic) NSArray<NSDictionary*>* items;
@property (strong, nonatomic) NSNumber* lastOnlineTime;

@end
