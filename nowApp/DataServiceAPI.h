//
//  DataServiceAPI.h
//  nowApp
//
//  Created by Leonardo Amigoni on 1/25/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>
#import <UIKit/UIKit.h>
#import "User.h"
#import "UserSearch.h"

@interface DataServiceAPI : NSObject

+(DataServiceAPI*) sharedManager;

@property Firebase *ROOT_REF;
@property Firebase *REF_USERS;
@property Firebase *REF_CURRENT_USER;
@property NSString *uid;
@property User *currentUserProfileInfo;

//Authorization Methods
- (void)registerUserWithEmail:(NSString *)email password:(NSString *)password firstName: (NSString*) firstName lastName: (NSString *)lastName bio:(NSString *)bio completion:(void (^)(BOOL))completion;

//Following Methods
- (void) startFollowingUser:(NSString *)userId withBlock:(void (^)())block;
- (void) stopFollowingUser:(NSString *)userId withBlock:(void (^)())block;
- (BOOL)isContactFollowed: (NSString *)userID;
//CRUD Methods
- (void) getFeedData: (void (^)(NSMutableArray *data))block;
- (void) getCurrentUserData;
- (User *)parseUserData: (FDataSnapshot *)data;
- (NSMutableArray *) parseItemsIntoArray: (NSArray *)itemsData; //TO FIX JSONMODEL BUG
- (void) updateProfile: (NSString *) firstName lastName: (NSString *)lastName bio: (NSString *)bio website: (NSString *) website;
- (void) saveItemData:(NSString *)key text: (NSString *) text url: (NSString *)url position: (NSString *)position completion:(void (^)())block;
- (void) saveLastOnlineTime;
- (void) uploadProfilePicture: (UIImage *)image;

@end
