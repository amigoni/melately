//
//  IntroRootViewController.h
//  nowApp
//
//  Created by Leonardo Amigoni on 2/2/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroRootViewController : UIViewController <UIPageViewControllerDataSource>

- (void)goToPreviousContentViewController;
- (void)goToNextContentViewController;

@end
