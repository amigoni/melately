//
//  IntroFinalViewController.h
//  nowApp
//
//  Created by Leonardo Amigoni on 2/2/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseContentViewController.h"

@interface IntroFinalViewController : BaseContentViewController

@property (weak, nonatomic) IBOutlet UIButton *getStartedButton;

@end
