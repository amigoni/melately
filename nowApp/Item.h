//
//  Item.h
//  nowApp
//
//  Created by Leonardo Amigoni on 2/10/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface Item : JSONModel

//@property (strong, nonatomic) NSString* key;
@property (strong, nonatomic) NSString* text;
@property (strong, nonatomic) NSString* url;
@property (nonatomic) NSNumber* position;
@property (nonatomic) NSNumber* updateTime;

@end
