//
//  ViewController.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "FeedViewController.h"
#import "UpdateStatusViewController.h"
#import "DataServiceAPI.h"
#import "PersonTableViewCell.h"
#import "User.h"

@interface FeedViewController ()

@end

@implementation FeedViewController {
    BOOL pressedInfoButton;
    DataServiceAPI *dataServiceAPI;
    NSMutableArray *tableData;
    NSMutableArray *usersKeys;
    NSIndexPath *expandedIndexPath;
}

//REMEMBER TO CLEAR ALL REFS AND OBSERVERS

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //self.title = @"MeLately";
    pressedInfoButton = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 200;
    
    [super viewDidLoad];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    dataServiceAPI = [DataServiceAPI sharedManager];
    tableData = [[NSMutableArray alloc] init];
    
    
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:255.0/255.0 green:191.0/255.0 blue:0.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                           nil, nil,
                                                           [UIFont fontWithName:@"Helvetica" size:21.0], NSFontAttributeName, nil]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onIntroClosed)
                                                 name:@"IntroClosed"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getFeedData)
                                                 name:@"UpdatedFollowing"
                                               object:nil];
    
    [dataServiceAPI getFeedData:^(NSMutableArray* data){
        tableData = data;
        [self.tableView reloadData];
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)onIntroClosed {
    // If it's the first time you open the app go to the UpdateStatus page
    // Otherwise if it's from the info button go to the feed
    
    if (pressedInfoButton == NO) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UpdateStatusViewController *newVC = [storyboard instantiateViewControllerWithIdentifier:@"UpdateStatusViewController"];
        [self presentViewController:newVC animated:NO completion:nil];
    }
    
    pressedInfoButton = NO;
}

- (void)getFeedData{
    [dataServiceAPI getFeedData:^(NSMutableArray* data){
        tableData = data;
        [self.tableView reloadData];
    }];
}



#pragma mark - Button Actions

- (IBAction)onFindFriendsButtonPressed:(id)sender {

}


- (IBAction)onUpdateStatusButtonPressed:(id)sender {

}


- (IBAction)onPatronButtonPressed:(id)sender {

}


- (IBAction)onInfoButtonPressed:(id)sender {
    pressedInfoButton = YES;
}



#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonTableViewCell"];
    
    if (!cell) {
        cell = [[PersonTableViewCell alloc] init];
    }
    
    [cell configureCell: tableData[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   [tableView beginUpdates];
    
    PersonTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell toggleCellExpansion];
    
    /*
    //Can controll the animation but doesn't work that well
    if (cell.isExpanded == YES) {
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
    } else {
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
    }
    */
    
    
    if ([indexPath compare:expandedIndexPath] == NSOrderedSame) {
        expandedIndexPath = nil;
    } else {
        expandedIndexPath = indexPath;
    }
    
    [tableView endUpdates];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [dataServiceAPI getFeedData:^(NSMutableArray* data){
        tableData = data;
        [refreshControl endRefreshing];
        [self.tableView reloadData];
    }];
}

@end
