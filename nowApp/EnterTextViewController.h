//
//  EnterTextViewController.h
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterTextViewController : UIViewController <UITextViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSString *position;
@property int itemNumber;


@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *postButton;

@property (weak, nonatomic) IBOutlet UILabel *textViewHintLabel;
@property (weak, nonatomic) IBOutlet UILabel *charCounterLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (strong, nonatomic) void(^completion)();


@end
