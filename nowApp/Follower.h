//
//  Followers.h
//  nowApp
//
//  Created by Leonardo Amigoni on 2/10/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "JSONModel.h"

@interface Follower : JSONModel

@property (strong, nonatomic) NSString *uid;

@end
