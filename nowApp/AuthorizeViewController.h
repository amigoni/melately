//
//  AuthorizeViewController.h
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "FeedViewController.h"

@interface AuthorizeViewController : FeedViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *bioTextField;
@property (weak, nonatomic) IBOutlet UIButton *instructionsButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpLoginSwitchButton;

@end
