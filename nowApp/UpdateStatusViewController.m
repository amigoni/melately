//
//  UpdateStatusViewController.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "UpdateStatusViewController.h"
#import "ItemTableViewCell.h"
#import "DataServiceAPI.h"
#import "PatronTableViewController.h"
#import "ProfileViewController.h"
#import "EnterTextViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>


@implementation UpdateStatusViewController{
    DataServiceAPI *dataServiceAPI;
    NSMutableArray *sortedData; //sorted
    NSMutableArray *fetchedData;
    Firebase *itemsRef;
    FirebaseHandle itemsObserver;
    Firebase *profileRef;
    FirebaseHandle profileObserver;
    NSNotificationCenter *notificationCenter;
}

@dynamic tableView;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"What are you up to lately?";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setEditing:YES];
    dataServiceAPI = [DataServiceAPI sharedManager];
    
        /*
     NSSortDescriptor *sortByPosition = [NSSortDescriptor sortDescriptorWithKey:@"position"
     ascending:YES];
     NSArray *sortDescriptors = [NSArray arrayWithObject:sortByPosition];
     sortedData = (NSMutableArray *)[fetchedData sortedArrayUsingDescriptors:sortDescriptors];
     */
    //sortedData = [NSMutableArray arrayWithArray:dataServiceAPI.currentUserProfileInfo.items];
    sortedData = [dataServiceAPI parseItemsIntoArray:dataServiceAPI.currentUserProfileInfo.items];
    [self.tableView reloadData];
    
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self action:@selector(longPressGestureRecognized:)];
    [self.tableView addGestureRecognizer:longPress];
    
    [_patronButton.layer setBorderWidth:3.0];
    [_patronButton.layer setCornerRadius:10.0];
    [_patronButton.layer setBorderColor:[[UIColor colorWithRed:183.0/255 green:54.0/255 blue:40.0/255 alpha:1.0] CGColor]];
    
    //Configure Profile Area
    [_profileButton.layer setCornerRadius:10.0];
    
    [self updateProfileInfo];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProfileInfo)
                                                 name:@"CurrentUserUpdated"
                                               object:nil];
}


- (void)viewDidDisappear:(BOOL)animated{
    [itemsRef removeObserverWithHandle:itemsObserver];
    [notificationCenter removeObserver:self name:@"CurrentUserUpdated" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateProfileInfo {
    self.profileNameLabel.text = [NSString stringWithFormat:@"%@ %@", dataServiceAPI.currentUserProfileInfo.profileInfo.firstName, dataServiceAPI.currentUserProfileInfo.profileInfo.lastName];
    self.profileDescriptionLabel.text = dataServiceAPI.currentUserProfileInfo.profileInfo.bio;
    self.followersLabel.text = [NSString stringWithFormat:@"Followers: %@", [@(dataServiceAPI.currentUserProfileInfo.followers.count) stringValue]];
    self.followingLabel.text = [NSString stringWithFormat:@"Following: %@", [@(dataServiceAPI.currentUserProfileInfo.following.count) stringValue]];
    
    [self.profileImage sd_setImageWithURL: [NSURL URLWithString:dataServiceAPI.currentUserProfileInfo.profileInfo.pictureURL]
                         placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    if (error) {
                                        NSLog(@"Error Fetching Picture%@",error.description);
                                    }
                                }];
}

#pragma - ItemsTableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_itemsTableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [sortedData count];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Item *itemData = sortedData[indexPath.row];
 
    ItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StatusItemTableViewCell"];
    
    if (!cell) {
        cell = [[ItemTableViewCell alloc] init];
    }
 
    [cell configureCell:[NSString stringWithFormat:@"item%i", (int)indexPath.row]position:indexPath.row text:itemData.text website:itemData.url updateTime: (NSString*)itemData.updateTime];
    
    //WARNING: Pushing a reference to the data here but should probably just have a completion block
    cell.itemsData = sortedData;
 
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Item *itemData = sortedData[indexPath.row];
    
    //Open Entertext
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EnterTextViewController *newVC = [storyboard instantiateViewControllerWithIdentifier:@"EnterTextViewController"];
    __weak EnterTextViewController* weakNewVC = newVC;
    
    weakNewVC.titleLabel.text = @"Enter text";
    
    weakNewVC.key = [NSString stringWithFormat:@"item%i",(int)indexPath.row];
    weakNewVC.position = [NSString stringWithFormat:@"%i",(int)indexPath.row];
    weakNewVC.text = itemData.text;
    weakNewVC.url = itemData.url;
    weakNewVC.completion = ^{
        itemData.text = weakNewVC.textView.text;
        itemData.url = weakNewVC.textField.text;
        NSDate *now = [[NSDate alloc]init];
        long nowNumber = (long)now.timeIntervalSince1970*1000; //Should probably add the ServerTimeDifference here
        itemData.updateTime = [NSNumber numberWithLong:nowNumber];
        
        [tableView reloadData];
    };
    
    [self presentViewController:weakNewVC animated:YES completion:nil];
}

- (IBAction)longPressGestureRecognized:(id)sender {
    /*
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = longPress.state;
    
    CGPoint location = [longPress locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    static UIView       *snapshot = nil;        ///< A snapshot of the row user is moving.
    static NSIndexPath  *sourceIndexPath = nil; ///< Initial index path, where gesture begins.
    
    switch (state) {
        case UIGestureRecognizerStateBegan: {
            if (indexPath) {
                sourceIndexPath = indexPath;
                
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                
                // Take a snapshot of the selected row using helper method.
                snapshot = [self customSnapshoFromView:cell];
                
                // Add the snapshot as subview, centered at cell's center...
                __block CGPoint center = cell.center;
                snapshot.center = center;
                snapshot.alpha = 0.0;
                [self.tableView addSubview:snapshot];
                [UIView animateWithDuration:0.25 animations:^{
                    
                    // Offset for gesture location.
                    center.y = location.y;
                    snapshot.center = center;
                    snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                    snapshot.alpha = 0.98;
                    cell.alpha = 0.0;
                    
                } completion:^(BOOL finished) {
                    
                    cell.hidden = YES;
                    
                }];
            }
            break;
        }
            
        case UIGestureRecognizerStateChanged: {
            CGPoint center = snapshot.center;
            center.y = location.y;
            snapshot.center = center;
            
            // Is destination valid and is it different from source?
            if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
                
                // ... update data source.
                [sortedData exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                
                // ... move the rows.
                [self.tableView moveRowAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                
                // ... and update source so it is in sync with UI changes.
                sourceIndexPath = indexPath;
            }
            
            break;
        }
            
        default: {
            // Clean up.
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:sourceIndexPath];
            cell.hidden = NO;
            cell.alpha = 0.0;
            
            [UIView animateWithDuration:0.25 animations:^{
                
                snapshot.center = cell.center;
                snapshot.transform = CGAffineTransformIdentity;
                snapshot.alpha = 0.0;
                cell.alpha = 1.0;
                
            } completion:^(BOOL finished) {
                
                sourceIndexPath = nil;
                [snapshot removeFromSuperview];
                snapshot = nil;
                
            }];
            [self.tableView reloadData];
            break;
        }
    }
     */
}

- (UIView *)customSnapshoFromView:(UIView *)inputView {
    
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    
    return snapshot;
}

- (void)refreshTableData{
    //Get a reference to each tableViewcell
    //Iterate throught the cells and set the data to the values in the fields
}


# pragma mark - Button Actions

- (IBAction)onCloseButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onSaveButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    NSMutableDictionary *dataToSave = [[NSMutableDictionary alloc]init];
    
    int index = 1;
    for (NSMutableArray *item in sortedData) {
        NSDictionary *itemData = @{@"position":[item valueForKey:@"position"],@"text":[item valueForKey:@"text"],@"url":[item valueForKey:@"url"],@"updateTime":@""};
        
        NSString *key = [NSString stringWithFormat:@"item%d",index];
        [dataToSave setValue: itemData forKey:key];
        index++;
    }
    
    NSLog(@"%@",dataToSave);
    
    [itemsRef setValue:dataToSave withCompletionBlock:^(NSError *error,Firebase *itemsRef ){
        if (error) {
            NSLog(@"Data could not be saved.");
            // an error occurred while attempting login
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upps! Something is wrong!"
                                                            message:@"Please try to save again!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        } else {
            NSLog(@"Data saved successfully.");
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

- (IBAction)onPatronButtonPressed:(id)sender {
    // user is logged in, check authData for data
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PatronTableViewController *newVC = [storyboard instantiateViewControllerWithIdentifier:@"PatronNavigationController"];
    [self presentViewController:newVC animated:YES completion:nil];
}

- (IBAction)onProfileButtonPressed:(id)sender {
    // user is logged in, check authData for data
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *newVC = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    [self presentViewController:newVC animated:YES completion:nil];
}


@end
