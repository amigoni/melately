//
//  PersonSearch.h
//  nowApp
//
//  Created by Leonardo Amigoni on 2/10/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "JSONModel.h"

@interface UserSearch : JSONModel

//@property (strong, nonatomic) NSString* key;
@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (strong, nonatomic) NSString* uid;
@property (strong, nonatomic) NSString* pictureURL;
@property BOOL isFollowed; //Optional in Implementation
@property (nonatomic) NSNumber* followersCount;

@end
