//
//  BaseContentViewController.h
//  StoryboardPagingDemo
//
//  Created by Derek Lee on 2/6/15.
//  Copyright (c) 2015 Derek Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntroRootViewController.h"

@interface BaseContentViewController : UIViewController

#pragma mark - Properties
@property (nonatomic, weak) IntroRootViewController *rootViewController;

@end
