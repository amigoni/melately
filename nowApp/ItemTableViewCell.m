//
//  ItemTableViewCell.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/25/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "ItemTableViewCell.h"
#import <DateTools/DateTools.h>


@implementation ItemTableViewCell{
    NSInteger positionLocal;
    NSString *keyName;
}

@synthesize textLabel; //Don't know what it won't autosynthesize but this fixes the issue.

- (void)awakeFromNib {
    // Initialization code
    self.hintLabel.userInteractionEnabled = NO;
    self.textLabel.userInteractionEnabled = NO;
    self.updateTimeLabel.userInteractionEnabled = NO;
    self.numberLabel.userInteractionEnabled = NO;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)configureCell:(NSString *)key position:(NSInteger)position text:(NSString *)text website:(NSString *)website updateTime:(NSNumber *)updateTime{
    
    if ([text isEqualToString:@""]) {
        self.hintLabel.hidden = false;
    } else {
        self.hintLabel.hidden = true;
    }
    
    positionLocal = position;
    self.numberLabel.text = [NSString stringWithFormat:@"%ld",((long)position+1)];
    self.textLabel.text = text;
    
    if (website.length > 0) {
        NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
        self.textLabel.attributedText = [[NSAttributedString alloc] initWithString:text attributes:underlineAttribute];
    } else {
        self.textLabel.text = text;
    }

    self.updateTimeLabel.text = [self processString:updateTime];
}


- (NSString*) processString: (NSNumber *)updateTime{
    long long time = [updateTime longValue]/1000;
    NSDate *timeAgoDate = [NSDate dateWithTimeIntervalSince1970:time];
    //NSLog(@"Time Ago: %@", timeAgoDate.timeAgoSinceNow);
    //NSLog(@"Time Ago: %@", timeAgoDate.shortTimeAgoSinceNow);
    return timeAgoDate.shortTimeAgoSinceNow;
}


- (void)textFieldDidChange {
    [_itemsData[positionLocal-1] setValue:self.textLabel.text forKey:@"text"];
    [_itemsData[positionLocal-1] setValue:self.urlCopy forKey:@"url"];
    //NSLog(@"",_itemsData[positionLocal-1]);
}


@end
