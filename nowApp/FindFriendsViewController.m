//
//  FindFriendsViewController.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "FindFriendsViewController.h"
#import "DataServiceAPI.h"
#import "FindFriendsPersonTableViewCell.h"

@implementation FindFriendsViewController{
    DataServiceAPI *dataServiceAPI;
    NSMutableArray *tableData;
    NSMutableArray *usersID;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    tableData = [[NSMutableArray alloc] init];
    usersID = [[NSMutableArray alloc]init];
    dataServiceAPI = [DataServiceAPI sharedManager];
    
    //Getting all users for now TO CHANGE
    [[dataServiceAPI.REF_USERS queryOrderedByChild:@"firstName"]
     observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
         NSMutableDictionary *user = snapshot.value;
         if (![user[@"profileInfo"][@"uid"] isEqualToString:dataServiceAPI.uid] ) {
             [tableData addObject:user];
             [usersID addObject:snapshot.key];
             [_tableView reloadData];
         }
     }];
}

- (void)viewDidDisappear:(BOOL)animated
{
    
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *rowData = tableData[indexPath.row][@"profileInfo"];
    NSString *otherUserID = usersID[indexPath.row];
    
    FindFriendsPersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FindFriendsPersonTableViewCell"];
    
    if (!cell) {
        cell = [[FindFriendsPersonTableViewCell alloc]init];
    }
    
    [cell configure:rowData[@"firstName"] lastName:rowData[@"lastName"] bio:rowData[@"bio"] pictureURL:rowData[@"pictureURL"] isRegistered:YES socialNetwork:none isFollowed:[dataServiceAPI isContactFollowed:otherUserID] userID:otherUserID];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tableData.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - Button Actions

- (IBAction)onCloseButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onSaveButtonPressed:(id)sender
{

}

- (IBAction)onFacebookButtonPressed:(id)sender
{
    //Link to Facebook
}

- (IBAction)onTwitterButtonPressed:(id)sender
{
    //Link to Twitter
}

- (IBAction)onContactsButtonPressed:(id)sender
{
    //Connect to Address Book
}

- (IBAction)onSearchButtonPressed:(id)sender
{
    //Open SearchFriendsViewController
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FeedViewController *newVC = [storyboard instantiateViewControllerWithIdentifier:@"SearchFriendsNavigationController"];
    [self presentViewController:newVC animated:NO completion:nil];
}

@end
