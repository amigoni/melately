//
//  PersonTableViewCell.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "PersonTableViewCell.h"
#import "ItemView.h"
#import "DataServiceAPI.h"
#import <DateTools/DateTools.h>
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PersonTableViewCell{
    DataServiceAPI *dataServiceAPI;
    NSMutableArray *itemsRef;
    int blueIndex; //iterator to vary blue shade
    int greyIndex; //iterator to vary gray shade
    BOOL oldItemsHidden; //Temp local variable to hide old items
    NSArray *sortedItemsData;
}


- (void)awakeFromNib {
    // Initialization code
    itemsRef = [[NSMutableArray alloc]init];
    dataServiceAPI = [DataServiceAPI sharedManager];
    oldItemsHidden = NO;
    self.isExpanded = NO;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self addItems];
}


- (void)addItems{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    for (int i = 0; i < 5; i++) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ItemView" owner:self options:nil];
        
        UIView *itemView = [[UIView alloc] init];
        itemView = (UIView *)[nib objectAtIndex:0];
        itemView.layer.cornerRadius = 5;
        
        /*
        [self.itemsStack addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[itemView]|"
                                                                         options:0
                                                                         metrics:nil
                                                                           views:NSDictionaryOfVariableBindings(itemView)]];
         */
        
        //Can't seem to make the constraints work so setting the width to the screen size
        [itemView.widthAnchor constraintEqualToConstant:screenRect.size.width].active = true;
        itemView.translatesAutoresizingMaskIntoConstraints = false;
        self.itemsStack.translatesAutoresizingMaskIntoConstraints = false;
        [self.itemsStack addArrangedSubview:itemView];
        
        [itemsRef addObject:itemView];
    }
    
}


- (void)configureCell: (User *)data {
    blueIndex = 0;
    greyIndex = 0;
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@", data.profileInfo.firstName, data.profileInfo.lastName];
    self.bioLabel.text = data.profileInfo.bio;
    self.personalWebsiteLabel.text = data.profileInfo.website;
    
    long time = [data.profileInfo.updateTime longValue]/1000;
    NSDate *timeAgoDate = [NSDate dateWithTimeIntervalSince1970:time];
    self.dateLabel.text = timeAgoDate.shortTimeAgoSinceNow;
    
    [self.icon sd_setImageWithURL: [NSURL URLWithString:data.profileInfo.pictureURL]
                         placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                options:SDWebImageRefreshCached
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    if (error) {
                                        NSLog(@"Error Fetching Picture%@",error.description);
                                    }
                                }];
    
    long lastOnlineTime = [dataServiceAPI.currentUserProfileInfo.lastOnlineTime longValue];
    
    sortedItemsData = [self sortArrayBy:[dataServiceAPI parseItemsIntoArray:data.items] sortValue:@"updateTime"];
    
    //Configure ItemViews
    int i = 0;
    for(ItemView *itemView in itemsRef){
        Item *itemData = sortedItemsData[i];
        long updateTime = [itemData.updateTime longValue];
        
        if (updateTime >= lastOnlineTime ) {
            [itemView configure:itemData isNew:YES];
            itemView.backgroundColor = [UIColor colorWithRed:0.0 green:(100.0+blueIndex*15)/255 blue:(135.0+blueIndex*15)/255 alpha:1.0];
            
            blueIndex++;
        } else {
            [itemView configure:itemData isNew:NO];
            itemView.backgroundColor = [UIColor colorWithRed:(185.0+greyIndex*5)/255 green:(185.0+greyIndex*5)/255 blue:(185.0+greyIndex*5)/255 alpha:1.0];
            
            greyIndex++;
        }
        
        i++;
    }
    
    [self checkIfItemsAreHidden];
    
    if (self.isExpanded == NO){
        self.moreDotsView.hidden = false;
    } else {
        self.moreDotsView.hidden = true;
    }

    //Need to hyperlink all the texts too if necessary
}


-(void) checkIfItemsAreHidden {
    long lastOnlineTime = [dataServiceAPI.currentUserProfileInfo.lastOnlineTime longValue];
    int i = 0;
    for(ItemView *itemView in itemsRef){
        Item *itemData = sortedItemsData[i];
        //Leave the first row always visible
        if (itemData.text && ![itemData.text isEqualToString:@""]) {
            if (i>=1) {
                if (self.isExpanded == NO){
                    itemView.hidden = true;
                    if (oldItemsHidden == YES && [itemData.updateTime longValue]<= lastOnlineTime) {
                        itemView.hidden = true;
                    }
                } else {
                    itemView.hidden = false;
                }
            }
        } else {
            itemView.hidden = true;
        }
        i++;
    }
}


-(void) toggleCellExpansion {
    if (self.isExpanded == NO){
        self.isExpanded = YES;
        self.moreDotsView.hidden = true;
    } else {
        self.isExpanded = NO;
        self.moreDotsView.hidden = false;
    }
    
    [self checkIfItemsAreHidden];
}


- (NSArray*) sortArrayBy: (NSMutableArray*)unsortedArray sortValue:(NSString *)sortValue {
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortValue
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    return  [unsortedArray sortedArrayUsingDescriptors:sortDescriptors];
}

#pragma mark - Button Actions

@end
