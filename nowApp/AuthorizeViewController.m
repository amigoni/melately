//
//  AuthorizeViewController.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "AuthorizeViewController.h"
#import "DataServiceAPI.h"
#import "FeedViewController.h"
#import "ProfileViewController.h"

@interface AuthorizeViewController ()

@end


@implementation AuthorizeViewController{
    DataServiceAPI *dataServiceAPI;
    BOOL isSignUpState;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dataServiceAPI = [DataServiceAPI sharedManager];
    isSignUpState = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)loginEmail: (NSString*) email password: (NSString*) password{
    [dataServiceAPI.ROOT_REF authUser:email
                             password:password
                             withCompletionBlock:^(NSError *error1, FAuthData *authData) {
                                 
                                 if (error1) {
                                     // an error occurred while attempting login
                                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upps! Something is wrong!"
                                                                          message:error1.description
                                                                          delegate:nil
                                                                          cancelButtonTitle:@"OK"
                                                                           otherButtonTitles:nil];
                                     [alert show];
                                 } else {
                                     NSString *uid = authData.uid;
                                     [[NSUserDefaults standardUserDefaults] setValue:uid forKey:@"uid"];
                                     [self openFeed];
                                 }
                                 
                             }];
}


- (void) openFeed {
    // user is logged in, check authData for data
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FeedViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"FeedViewController"];
    [self presentViewController:sfvc animated:YES completion:nil];
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


- (NSString *)stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:(NSString *)string
{
    NSString *leadingTrailingWhiteSpacesPattern = @"(?:^\\s+)|(?:\\s+$)";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:leadingTrailingWhiteSpacesPattern options:NSRegularExpressionCaseInsensitive error:NULL];
    
    NSRange stringRange = NSMakeRange(0, string.length);
    NSString *trimmedString = [regex stringByReplacingMatchesInString:string options:NSMatchingReportProgress range:stringRange withTemplate:@"$1"];
    
    return trimmedString;
}

- (void)showAlert:(NSString *)description{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upps! Something is wrong!"
                                                    message:description
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


- (BOOL)validateString: (NSString *)string pattern:(NSCharacterSet *)pattern{
    NSCharacterSet *set = [NSCharacterSet letterCharacterSet];
    return [[string stringByTrimmingCharactersInSet:set] isEqualToString:@""];
}

#pragma mark - Button Action

- (IBAction)onSignUpPressed:(id)sender {
    if (isSignUpState) {
        BOOL formIsWellCompleted = true;
        NSString *errorDescription = @"";
        NSString *email = [self stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:self.emailTextField.text];
        NSString *firstName = [self stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:self.firstNameTextField.text];
        NSString *lastName = [self stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:self.lastNameTextField.text];
        NSString *bio = [self stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:self.bioTextField.text];
        
        if (![self validateEmailWithString:email]) {
            formIsWellCompleted = false;
            //Alert not a valid email
            errorDescription = [errorDescription stringByAppendingString:@"NOT A VALID EMAIL"];
            NSLog(@"Not a valid email");
        }
        
        if (self.passwordTextField.text.length < 8 || self.passwordTextField.text.length > 16) {
            formIsWellCompleted = false;
            errorDescription = [errorDescription stringByAppendingString:@"\nPassword between 8 and 16 characters"];
            NSLog(@"Password between 8 and 16 characters");
        }
        
        if (firstName.length < 1) {
            formIsWellCompleted = false;
            errorDescription = [errorDescription stringByAppendingString:@"\nNeed a first name"];
            NSLog(@"Need a first name");
        }
        
        
        if(![self validateString:firstName pattern:[NSCharacterSet letterCharacterSet]]){
            formIsWellCompleted = false;
            errorDescription = [errorDescription stringByAppendingString:@"\nFirst name can only use letters"];
            NSLog(@"First name can only use letters");
        }
        
        if (lastName.length < 1) {
            formIsWellCompleted = false;
            errorDescription = [errorDescription stringByAppendingString:@"\nNeed a last name"];
            NSLog(@"Need a last name");
        }
        
        if(![self validateString:lastName pattern:[NSCharacterSet letterCharacterSet]]){
            formIsWellCompleted = false;
            errorDescription = [errorDescription stringByAppendingString:@"\nLast name can only use letters"];
            NSLog(@"Last name can only use letters");
        }
        
        if(![self validateString:bio pattern:[NSCharacterSet alphanumericCharacterSet]]){
            formIsWellCompleted = false;
            errorDescription = [errorDescription stringByAppendingString:@"\nBio can only be alphanumeric"];
            NSLog(@"Bio can only be alphanumeric");
        }
        
        if(formIsWellCompleted){
            [dataServiceAPI registerUserWithEmail:email
                                         password:self.passwordTextField.text
                                        firstName:firstName
                                         lastName:lastName
                                              bio:bio
                                       completion:^(BOOL finished){
                
                [self loginEmail:_emailTextField.text password:_passwordTextField.text];
            }];
        } else {
            [self showAlert:errorDescription];
        }
    }
    else {
       [self loginEmail:_emailTextField.text password:_passwordTextField.text];
    }
}


- (IBAction)onFacebookButtonPressed:(id)sender {
    
}


- (IBAction)onLoginSignupSwitchPressed:(id)sender {
   
    if (isSignUpState == YES) {
        isSignUpState = NO;
        self.titleLabel.text = @"Login";
        [self.instructionsButton setTitle:@"Forgot your password? Click here" forState:UIControlStateNormal];
        [self.instructionsButton setEnabled:YES];
        
        [self.signUpButton setTitle:@"Login" forState:UIControlStateNormal];
        
        [self.signUpLoginSwitchButton setTitle:@"Don't have an account? Click here" forState:UIControlStateNormal];
        self.firstNameTextField.hidden = true;
        self.lastNameTextField.hidden = true;
        self.bioTextField.hidden = true;
        
    } else {
        isSignUpState = YES;
        self.titleLabel.text = @"Create an account";
        [self.instructionsButton setTitle:@"*required" forState:UIControlStateNormal];
        [self.instructionsButton setEnabled:NO];
        
        [self.signUpButton setTitle:@"Signup" forState:UIControlStateNormal];
        
        [self.signUpLoginSwitchButton setTitle:@"Already have an account? Click here" forState:UIControlStateNormal];
        self.firstNameTextField.hidden = false;
        self.lastNameTextField.hidden = false;
        self.bioTextField.hidden = false;
    }

}


- (IBAction)onInstructionsButtonPressed:(id)sender {
    
}


@end
