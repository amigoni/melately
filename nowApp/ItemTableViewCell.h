//
//  ItemTableViewCell.h
//  nowApp
//
//  Created by Leonardo Amigoni on 1/25/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *hintLabel;
@property (weak, nonatomic) IBOutlet UILabel *updateTimeLabel;
@property (weak, nonatomic) NSMutableArray *itemsData;
@property (weak, nonatomic) NSString *urlCopy;

- (void)configureCell: (NSString *)key position:(NSInteger) position text:(NSString *)text  website: (NSString *)website updateTime: (NSString *)updateTime;
-(void) textFieldDidChange;

@end
