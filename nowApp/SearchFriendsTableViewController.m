//
//  SearchFriendsTableViewController.m
//  nowApp
//
//  Created by Leonardo Amigoni on 2/9/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "SearchFriendsTableViewController.h"
#import "FindFriendsPersonTableViewCell.h"
#import "DataServiceAPI.h"


@interface SearchFriendsTableViewController ()

@property (strong, nonatomic) UISearchController *searchController;
@property NSMutableArray *people;
@property NSMutableArray *searchResults;

@end


@implementation SearchFriendsTableViewController{
    DataServiceAPI *dataServiceAPI;
    NSString *currentSearchString;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dataServiceAPI = [DataServiceAPI sharedManager];
    
    self.people = [[NSMutableArray alloc]init];
    self.searchResults = [[NSMutableArray alloc]init];
    
    //Setup the Search Controller
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.searchController.searchBar.translucent = false;
    self.searchController.searchBar.placeholder = @"enter a name";
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchController.hidesNavigationBarDuringPresentation = false;
    
    //Customize Navigation bar
    self.title = @"Search Friends";
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Helvetica" size:15.0], NSFontAttributeName, nil]];
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:255.0/255 alpha:1]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserSearch *personData = self.searchResults[indexPath.row];
    
    FindFriendsPersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FindFriendsPersonTableViewCell"];
    
    if (!cell) {
        cell = [[FindFriendsPersonTableViewCell alloc]init];
    }
    
    [cell configure:personData.firstName lastName:personData.lastName bio:@"" pictureURL:personData.pictureURL isRegistered:YES socialNetwork:none isFollowed:personData.isFollowed userID:personData.uid];
    
    return cell;
}

#pragma mark - Searchbar Methods

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = [searchController.searchBar.text lowercaseString];
    
    if (![searchString isEqualToString:currentSearchString]) {
        self.searchResults = [[NSMutableArray alloc]init];
        [self.tableView reloadData];
       
        if (searchString.length == 0) {
            //self.searchResults = self.people;
            //[self.tableView reloadData];
        }
        else if (searchString.length >= 1) {
            if (searchString.length < currentSearchString.length){
                
            }
            currentSearchString = searchString;

            NSString *stem = searchString;
            
            if (searchString.length >= 1) {
                Firebase *firstNameRef = [dataServiceAPI.ROOT_REF childByAppendingPath:@"/search/firstName"];
                [[[firstNameRef queryOrderedByKey] queryStartingAtValue:stem]observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                    NSArray* people = [snapshot.value allValues];
                    people = [[people reverseObjectEnumerator] allObjects];
                    
                    for (NSDictionary *person in people) {
                        NSError *err;
                        UserSearch* user = [[UserSearch alloc] initWithDictionary:person error:&err];
                        if (err) {
                            NSLog(@"%@",err.description);
                        }
                        if (![user.uid isEqualToString:dataServiceAPI.uid]) {
                            user.isFollowed = [dataServiceAPI isContactFollowed:user.uid];
                            [self.searchResults addObject:user];
                        }
                    }
                    
                    [self.tableView reloadData];
                }];
                /*
                 Firebase* lastNameRef = [dataServiceAPI.ROOT_REF childByAppendingPath:@"/search/lastName"];
                 
                 [[[lastNameRef queryOrderedByKey] queryStartingAtValue:stem] observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
                 
                 for (NSDictionary* person in snapshot.value) {
                 [self.searchResults addObject:person];
                 }
                 [self.tableView reloadData];
                 }];
                 */
            } else {
                [self searchForText:searchString];
                [self.tableView reloadData];
            }
            
            NSLog(@"updateSearchResultsForSearchController");
        }
    }
}

- (void)searchForText:(NSString*)searchText
{
    //Filter results both on firstName and lastName
    NSString* filter = @"%K CONTAINS[cd] %@ || %K CONTAINS[cd] %@";
    NSArray* args = @[@"firstName", searchText, @"lastName", searchText];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:filter argumentArray:args];
    self.searchResults = [NSMutableArray arrayWithArray:[self.people filteredArrayUsingPredicate:predicate]];
}


#pragma mark - Button Actions

- (IBAction)onClosePressed:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

@end
