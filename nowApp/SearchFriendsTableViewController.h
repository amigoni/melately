//
//  SearchFriendsTableViewController.h
//  nowApp
//
//  Created by Leonardo Amigoni on 2/9/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchFriendsTableViewController : UITableViewController <UISearchBarDelegate,UISearchControllerDelegate,UISearchResultsUpdating>

@end
