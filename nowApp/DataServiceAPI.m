//
//  DataServiceAPI.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/25/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#define NUMBER_OF_ITEMS 5

#import "DataServiceAPI.h"
#import "AppDelegate.h"
#import <AWSCore/AWSCore.h>
#import <AWSS3TransferManager.h>
#import <AWSS3/AWSS3.h>

@implementation DataServiceAPI


#pragma mark - Initialization methods

+ (DataServiceAPI*)sharedManager
{
    //Ensuring Singleton
    static DataServiceAPI *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[DataServiceAPI alloc] init];
    });
    return _sharedInstance;
}


- (id)init {
    if (self = [super init]) {
        _ROOT_REF = [[Firebase alloc] initWithUrl: @"https://nowapp.firebaseio.com/"];
        _REF_USERS = [_ROOT_REF childByAppendingPath:@"/users"];
        _uid = [[NSUserDefaults standardUserDefaults] valueForKey:@"uid"];
        if(_uid){
            _REF_CURRENT_USER = [_REF_USERS childByAppendingPath:_uid];
            //Get the user data to be accessed from anywhere
            [self getCurrentUserData];
            
            Firebase *userLastOnlineRef = [_REF_CURRENT_USER childByAppendingPath:@"lastOnlineTime"];
            [userLastOnlineRef onDisconnectSetValue:kFirebaseServerValueTimestamp];
            
            //AWS S3
            AWSRegionType const CognitoRegionType = AWSRegionUSEast1;
            AWSRegionType const DefaultServiceRegionType = AWSRegionUSEast1;
            NSString *const CognitoIdentityPoolId = @"us-east-1:6160916e-da35-4ef5-ab4a-8fa3ee5833a6";
            
            AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:CognitoRegionType
                identityPoolId:CognitoIdentityPoolId];
            AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:DefaultServiceRegionType credentialsProvider:credentialsProvider];
            AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
        }
    }
    
    return self;
}



#pragma mark - Authorization Methods

- (void)registerUserWithEmail:(NSString *)email password:(NSString *)password firstName: (NSString*) firstName lastName: (NSString *)lastName bio:(NSString *)bio completion:(void (^)(BOOL))completion {
    [self.ROOT_REF createUser:email
                    password:password
                    withValueCompletionBlock:^(NSError *error, NSDictionary *result) {
                       if (error) {
                           // There was an error creating the account
                           NSLog(@"Error creating the account: %@", error.description);
                           NSString *errorMessage = @"";
                           
                           if (error.code == -9) {
                               errorMessage = @"There is already an account with that email";
                           } else if (error.code == -6){
                               errorMessage = @"Invalid Password";
                           }
                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upps! Something is wrong!"
                                                                           message:errorMessage
                                                                          delegate:nil
                                                                 cancelButtonTitle:@"OK"
                                                                 otherButtonTitles:nil];
                           [alert show];
                       } else {
                           
                           //Add User Structure
                           NSString *uid = [result objectForKey:@"uid"];
                           self.uid = uid;
                           ProfileInfo * blankProfileInfo = [[ProfileInfo alloc] init];
                           blankProfileInfo.firstName = firstName;
                           blankProfileInfo.lastName = lastName;
                           blankProfileInfo.bio = bio;
                           blankProfileInfo.website = @"";
                           blankProfileInfo.pictureURL = @"";
                           
                           //Must do it manually since kFirebaseServerValueTimestamp is a dictionary and not a long long
                           NSDictionary *userBlankData = @{
                                                           @"profileInfo": @{
                                                                   @"uid":uid,
                                                                   @"firstName": blankProfileInfo.firstName,
                                                                             @"lastName": blankProfileInfo.lastName,
                                                                             @"bio": blankProfileInfo.bio,
                                                                             @"website": blankProfileInfo.website,
                                                                             @"pictureURL": blankProfileInfo.pictureURL,
                                                                             @"createdTime": kFirebaseServerValueTimestamp,
                                                                             @"updateTime": kFirebaseServerValueTimestamp,
                                                                             }
                                                        };
                           
                           self.REF_CURRENT_USER = [self.REF_USERS childByAppendingPath:uid];
                           [self.REF_CURRENT_USER setValue:userBlankData];
                           
                           //Populate search
                           [self populateSearchIndicesForUser:firstName lastName:lastName uid:uid pictureURL: @"" followersCount: 0];
                           
                           //Add Blank Items since they must have a key that is familiar
                           Item *emptyItem = [[Item alloc]init];
                           emptyItem.text = @"";
                           emptyItem.url = @"";
                           emptyItem.position = 0;
                           
                           
                           NSMutableArray *itemsData = [[NSMutableArray alloc] init];
                           for (int i = 0; i < NUMBER_OF_ITEMS; ++i) {
                               NSDictionary *emptyData = @{
                                                           @"text": emptyItem.text,
                                                           @"url": emptyItem.url,
                                                           @"position": [NSNumber numberWithInt:i],
                                                           @"updateTime": kFirebaseServerValueTimestamp
                                                           };

                               [itemsData addObject:emptyData];
                           }
                           
                           [[self.REF_CURRENT_USER childByAppendingPath:@"items"] setValue:itemsData];
                           
                           //Save this for the first time
                           [self saveLastOnlineTime];
                           
                            NSLog(@"Successfully created user account with uid: %@", uid);
                           
                           [self getCurrentUserData];
                           
                           completion(true);
                       }
    }];
}


- (void) saveLastOnlineTime {
    //Record the last time the person was online. With Firebase since it's realtime we want to record when a person has logged out in order to mark properly as new the new items. This assumes we update the feed live.
    Firebase *userLastOnlineRef = [_REF_CURRENT_USER childByAppendingPath:@"lastOnlineTime"];
    [userLastOnlineRef setValue:kFirebaseServerValueTimestamp];
}


# pragma mark - Following-Unfollowing

-(void) startFollowingUser:(NSString *)userId withBlock:(void (^)())block {
    //Add Followee to user following list
    Firebase *followingRef = [[_REF_CURRENT_USER childByAppendingPath:@"following"] childByAppendingPath:userId];
    
    [followingRef setValue:userId withCompletionBlock:^(NSError *error, Firebase* ref) {
        //Add user to the Followee followers list
        Firebase* followerRef = [[[_ROOT_REF childByAppendingPath:@"users"] childByAppendingPath: userId]childByAppendingPath:@"followers"];
        Firebase *newRef = [followerRef childByAppendingPath:self.uid];
        [newRef setValue:self.uid];
        
        //TODO decrease following count in search
        
        block();
    }];
}


- (void) stopFollowingUser:(NSString *)userId withBlock:(void (^)())block{
    //Remove Followee to user following list
    Firebase *followingRef = [[_REF_CURRENT_USER childByAppendingPath:@"following"] childByAppendingPath:userId];
    [followingRef removeValueWithCompletionBlock:^(NSError *error, Firebase* ref) {
        //Remove user to the Followee followers list
        Firebase* followerRef = [[_ROOT_REF childByAppendingPath:@"users"] childByAppendingPath:userId];
        
        [[[followerRef childByAppendingPath:@"followers"] childByAppendingPath:self.uid] removeValue];
        //TODO decrease following count in search

        block();
    }];
    
}

- (BOOL)isContactFollowed: (NSString *)userID {
        for (NSString *user in self.currentUserProfileInfo.following) {
            if ([userID isEqualToString:user]) {
                return YES;
            }
        }
    
    return NO;
}




#pragma mark - CRUD Methods

- (void)getFeedData:(void (^)(NSMutableArray *data))block{
    //Getting data for all the followers
    Firebase *followingRef = [self.REF_CURRENT_USER childByAppendingPath:@"/following"];
    __block NSMutableArray *feedData = [[NSMutableArray alloc] init];
    
    if (followingRef) {
        [[followingRef queryOrderedByKey] observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
            //Check if it's just an empty string and doesn't get auto converted to an array
            if (snapshot.exists && ![snapshot.value  isEqual: @""]) {
                NSMutableArray *followingUsers = [NSMutableArray arrayWithArray:[snapshot.value allValues]];
                NSMutableArray *followingUsersKeys = [NSMutableArray arrayWithArray:[snapshot.value allKeys]];
                if (followingUsersKeys.count > 0) {
                    __block int j = 0;
                    for (NSString *userID in followingUsers) {
                        //create an observer for each person the user is following and add the data to the tableData or replace it if it's been updated
                        // Right now the observer is on the whole user but should probably be optimized
                        
                        Firebase *followedUserRef = [self.REF_USERS childByAppendingPath:userID];
                        [followedUserRef observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot1) {
                            //Checking if the user already exists and in that case update it otherwise add it
                            if(snapshot1.key != self.uid){
                                BOOL userExists = false;
                                if (feedData.count > 0) {
                                    int i = 0;
                                    for (User *user in feedData) {
                                        if ([user.profileInfo.uid isEqualToString:snapshot1.key]) {
                                            [feedData removeObjectAtIndex:i];
                                            [feedData insertObject:[self parseUserData:snapshot1] atIndex:i];
                                            userExists = true;
                                            break;
                                        }
                                        i++;
                                    }
                                }
                                
                                if(!userExists) {
                                    [feedData addObject:[self parseUserData:snapshot1]];
                                }
                                
                                j++;
                                
                                //Call after the last callback
                                if (j == followingUsers.count) {
                                    block(feedData);
                                }
                            }
                        } withCancelBlock:^(NSError *error) {
                            NSLog(@"%@", error.description);
                        }];
                    }
                }
            } else {
                feedData = nil;
                feedData = [[NSMutableArray alloc] init];
                block(feedData);
            }
        }];
    }
}

- (void)getCurrentUserData {
    //Fetch User Data and keep it locally
    [self.REF_CURRENT_USER observeEventType:FEventTypeValue
                          withBlock:^(FDataSnapshot *snapshot) {
                              NSLog(@"%@", snapshot);
                              self.currentUserProfileInfo = [self parseUserData:snapshot];
                              [[NSNotificationCenter defaultCenter]
                               postNotificationName:@"CurrentUserUpdated"
                               object:self];
                          } withCancelBlock:^(NSError *error) {
                              NSLog(@"%@", error.description);
                          }
     ];
}

- (User *)parseUserData: (FDataSnapshot *)data {
    NSDictionary *profileInfo = [data.value valueForKey:@"profileInfo"];
    
    NSArray *items = [data.value valueForKey:@"items"];
    
    NSArray *following = [[data.value valueForKey:@"following"] allValues];
    NSArray *followers = [[data.value valueForKey:@"followers"] allValues];
    NSNumber *lastOnlineTime = [data.value valueForKey:@"lastOnlineTime"];
    
    NSMutableDictionary *cleanedData = [[NSMutableDictionary alloc] init];
    [cleanedData setValue: profileInfo forKey:@"profileInfo"];
    [cleanedData setValue: items forKey:@"items"];
    if (following.count > 0) {
        [cleanedData setValue: following forKey:@"following"];
    }
    if (followers.count > 0) {
        [cleanedData setValue: followers forKey:@"followers"];
    }
    
    [cleanedData setValue: followers forKey:@"followers"];
    [cleanedData setValue: lastOnlineTime forKey:@"lastOnlineTime"];
    
    
    //NSLog(@"%@", cleanedData);
    NSError* err = nil;
    
    User *returnObject = [[User alloc] initWithDictionary:cleanedData error:&err];
    
    if (err) {
        NSLog(@"Error parsing Profile Info %@", err.description);
    }

    return returnObject;
}

//MANUALLY CONVERT ITEMS (JSONMODEL BUG!!!!)
- (NSMutableArray *) parseItemsIntoArray: (NSArray *)itemsData {
    NSMutableArray *items = [[NSMutableArray alloc] init];
    for(NSDictionary *item in itemsData){
        NSError *err;
        Item *tempItem =[[Item alloc] initWithDictionary:item error:&err];
        if (err) {
            NSLog(@"Error parsing Item Info %@", err.description);
        }
        [items addObject:tempItem];
    }
    return items;
}


- (void) updateProfile: (NSString *) firstName lastName: (NSString *)lastName bio: (NSString *)bio website: (NSString *) website {
    
    NSString *oldFirstNameCopy = [NSString stringWithString:self.currentUserProfileInfo.profileInfo.firstName];
    NSString *oldLastNameCopy = [NSString stringWithString:self.currentUserProfileInfo.profileInfo.lastName];
    
    //1 Change the values in the profile info
    Firebase *profileInfoRef = [self.REF_CURRENT_USER childByAppendingPath:@"profileInfo" ];
    NSDictionary *profileData = @{
                                  @"uid": self.currentUserProfileInfo.profileInfo.uid,
                                  @"firstName" : firstName,
                                  @"lastName" : lastName,
                                  @"bio" : bio,
                                  @"website" : website,
                                  @"pictureURL" : self.currentUserProfileInfo.profileInfo.pictureURL,
                                  @"updateTime" : kFirebaseServerValueTimestamp,
                                  @"createdTime": self.currentUserProfileInfo.profileInfo.createdTime
                                  };
    
    
    [profileInfoRef setValue:profileData withCompletionBlock:^(NSError *error,Firebase *profileInfoRef ){
        if (error) {
            NSLog(@"Data could not be saved.");
            // an error occurred while attempting login
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upps! Something is wrong!"
                                                            message:@"Please try to save again!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            /*
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Upps! Something is wrong!"
                                          message:@"Please try to save again!"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
        
            [myCurrentController presentViewController:alert animated:YES completion:nil];
             
             //NEED A REFERENCE TO THE VIEWCONTROLLER
             */
            
        } else {
            //2 Remove names from search indeces
            [self removeSearchIndicesForUser:oldFirstNameCopy lastName:oldLastNameCopy uid:self.uid];
            //3 Add the them to the indeces
            [self populateSearchIndicesForUser:firstName lastName:lastName uid:self.uid pictureURL: [profileData valueForKey:@"pictureURL"] followersCount: self.currentUserProfileInfo.followers.count];
            NSLog(@"Data saved successfully.");
        }
    }];
}


- (void)saveItemData:(NSString *)key text:(NSString *)text url:(NSString *)url position:(NSString *)position completion:(void (^)())block{
    
    Firebase *itemsRef = [self.REF_CURRENT_USER childByAppendingPath:@"items"];
    
    NSDictionary *dataToSave = @{@"position":position,@"text":text,@"url":url,@"updateTime":kFirebaseServerValueTimestamp};
    
    NSLog(@"%@",dataToSave);
    
    [[itemsRef childByAppendingPath:position] setValue:dataToSave withCompletionBlock:^(NSError *error,Firebase *itemsRef ){
        if (error) {
            NSLog(@"Data could not be saved.");
            // an error occurred while attempting login
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upps! Something is wrong!"
                                                            message:@"Please try to save again!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        } else {
            NSLog(@"Data saved successfully.");
            
            //Update UpdateTime under user ProfileInfo
            Firebase *updateTimeRef = [[self.REF_CURRENT_USER childByAppendingPath:@"profileInfo"] childByAppendingPath:@"updateTime"];
            [updateTimeRef setValue:kFirebaseServerValueTimestamp];
            
            block();
        }
    }];
}

- (void)uploadProfilePicture:(UIImage *)image {
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    //convert uiimage to
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", self.uid]];
    [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
    
    NSURL* fileUrl = [NSURL fileURLWithPath:filePath];
    NSString *fileName = [NSString stringWithFormat:@"http://now-app-pictures.s3.amazonaws.com/%@_%li.png",self.uid,(long)[NSDate timeIntervalSinceReferenceDate]];
    
    //upload the image
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.body = fileUrl;
    uploadRequest.bucket = @"now-app-pictures";
    uploadRequest.key = [NSString stringWithFormat:@"%@_%li.png",self.uid,(long)[NSDate timeIntervalSinceReferenceDate]];
    uploadRequest.contentType = @"image/png";
    
    [[transferManager upload:uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
        if(task.error == nil) {
            NSLog(@"UPLOADED");
            Firebase *pictureRef = [[self.REF_CURRENT_USER childByAppendingPath:@"profileInfo" ] childByAppendingPath:@"pictureURL"];
            [pictureRef setValue:fileName withCompletionBlock:^(NSError *error,Firebase *profileInfoRef ){
                if (error) {
                    NSLog(@"Error setting pictureURL");
                }
            }];
        }
        return nil;
    }];    
}


#pragma mark - Search Helper Functions
//These methods are to create fast indexes to search users

- (void) removeSearchIndicesForUser: (NSString *)firstName lastName: (NSString *)lastName uid: (NSString *) uid {
    Firebase* firstNameRef = [self.ROOT_REF.root childByAppendingPath:@"search/firstName"];
    Firebase* lastNameRef = [self.ROOT_REF.root childByAppendingPath:@"search/lastName"];
    NSString* firstNameKey = [[NSString stringWithFormat:@"%@_%@_%@", firstName, lastName, uid] lowercaseString];
    NSString* lastNameKey = [[NSString stringWithFormat:@"%@_%@_%@", lastName, firstName, uid] lowercaseString];
    
    [[firstNameRef childByAppendingPath:firstNameKey] removeValue];
    [[lastNameRef childByAppendingPath:lastNameKey] removeValue];
}


- (void) populateSearchIndicesForUser: (NSString *)firstName lastName: (NSString *)lastName uid: (NSString *) uid pictureURL: (NSString *) pictureURL followersCount: (NSUInteger) followersCount{
    // For each user, we list them in the search index twice. Once by first name and once by last name. We include the id at the end to guarantee uniqueness
    Firebase* firstNameRef = [self.ROOT_REF.root childByAppendingPath:@"search/firstName"];
    Firebase* lastNameRef = [self.ROOT_REF.root childByAppendingPath:@"search/lastName"];
    NSString* firstNameKey = [[NSString stringWithFormat:@"%@_%@_%@", firstName, lastName, uid] lowercaseString];
    NSString* lastNameKey = [[NSString stringWithFormat:@"%@_%@_%@", lastName, firstName, uid] lowercaseString];
    
    NSDictionary *newData = @{@"firstName":firstName,
                              @"lastName":lastName,
                              @"pictureURL":pictureURL,
                              @"followersCount":[NSNumber numberWithInteger:followersCount],
                              @"uid": uid
                              };
    [[firstNameRef childByAppendingPath:firstNameKey] setValue:newData];
    [[lastNameRef childByAppendingPath:lastNameKey] setValue:newData];
}


@end
