//
//  ProfileViewController.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "ProfileViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "BASSquareCropperViewController.h"


@interface ProfileViewController () <BASSquareCropperDelegate>
@property (strong, nonatomic) UIImagePickerController *picker;
@end

@implementation ProfileViewController
{
    DataServiceAPI *dataServiceAPI;
    NSString *firstName;
    NSString *lastName;
    NSString *bio;
    NSString *website;
    Firebase *profileRef;
    FirebaseHandle profileObserver;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Your Profile";
    dataServiceAPI = [DataServiceAPI sharedManager];
    
    //Retrieve Profile Data and add Observer
    profileRef = [dataServiceAPI.REF_CURRENT_USER childByAppendingPath:@"profileInfo"];
    profileObserver = [profileRef observeEventType:FEventTypeValue
                                  withBlock:^(FDataSnapshot *snapshot) {
                                      NSLog(@"%@", snapshot.value);
                                      _firstNameTextField.text = [snapshot.value valueForKey:@"firstName"];
                                      _lastNameTextField.text = [snapshot.value valueForKey:@"lastName"];
                                      _bioTextField.text = [snapshot.value valueForKey:@"bio"];
                                      _webSiteTextField.text = [snapshot.value valueForKey:@"website"];
                                  } withCancelBlock:^(NSError *error) {
                                      NSLog(@"%@", error.description);
                                  }];
    
    [_patronButton.layer setBorderWidth:3.0];
    [_patronButton.layer setCornerRadius:10.0];
    [_patronButton.layer setBorderColor:[[UIColor colorWithRed:183.0/255 green:54.0/255 blue:40.0/255 alpha:1.0] CGColor]];
    

    [self.pictureImage sd_setImageWithURL: [NSURL URLWithString:dataServiceAPI.currentUserProfileInfo.profileInfo.pictureURL]
                         placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    if (error) {
                                        NSLog(@"Error Fetching Picture%@",error.description);
                                    }
                                }
     
     ];
    
}


- (void)viewDidDisappear:(BOOL)animated
{
    [profileRef removeObserverWithHandle:profileObserver];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)selectPhotos
{
    self.picker = [[UIImagePickerController alloc] init];
    __weak UIImagePickerController *weakPicker = self.picker;
    weakPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    weakPicker.delegate = self;
    [self presentViewController:weakPicker animated:YES completion:nil];
}



- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    BASSquareCropperViewController *squareCropperViewController = [[BASSquareCropperViewController alloc] initWithImage:image minimumCroppedImageSideLength:400.0f];
    squareCropperViewController.squareCropperDelegate = self;
    [picker pushViewController: squareCropperViewController animated:YES];
}


- (void)squareCropperDidCropImage:(UIImage *)croppedImage inCropper:(BASSquareCropperViewController *)cropper
{
    self.pictureImage.image = croppedImage;
    //UIImage *scaledImage200 = [self resizeImage:image scaledToSize:CGSizeMake(200, 200)];
    //[dataServiceAPI uploadProfilePicture:scaledImage200];
    UIImage *scaledImage80 = [self resizeImage:croppedImage scaledToSize:CGSizeMake(80, 80)];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CurrentUserUpdated" object:self];
    [dataServiceAPI uploadProfilePicture:scaledImage80];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)squareCropperDidCancelCropInCropper:(BASSquareCropperViewController *)cropper
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(UIImage*)resizeImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Button Actions

- (IBAction)onCloseButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onSaveButtonPressed:(id)sender
{
    [dataServiceAPI updateProfile:self.firstNameTextField.text lastName:self.lastNameTextField.text bio:self.bioTextField.text website:self.webSiteTextField.text];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onPictureButtonPressed:(id)sender
{
    [self selectPhotos];
}

- (IBAction)onPatronButtonPressed:(id)sender
{
    // user is logged in, check authData for data
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FeedViewController *newVC = [storyboard instantiateViewControllerWithIdentifier:@"PatronNavigationController"];
    [self presentViewController:newVC animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
