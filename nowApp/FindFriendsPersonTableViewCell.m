//
//  FindFriendsPersonTableViewCell.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/27/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "FindFriendsPersonTableViewCell.h"
#import "DataServiceAPI.h"

@implementation FindFriendsPersonTableViewCell{
    ButtonState buttonState;
    DataServiceAPI *dataServiceAPI;
    BOOL isFollowedCopy;
    BOOL isRegisteredCopy;
    NSString *userIDCopy;
}


- (void)awakeFromNib {
    // Initialization code
    dataServiceAPI = [DataServiceAPI sharedManager];
    _actionButton.layer.cornerRadius = 5;
    _actionButton.clipsToBounds = YES;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configure: (NSString *)firstName lastName:(NSString *)lastName bio:(NSString *)bio pictureURL:(NSString *)pictureURL isRegistered: (BOOL) isRegistered socialNetwork: (SocialNetwork) socialNetwork isFollowed: (BOOL) isFollowed userID: (NSString *)userID{
    
    isRegisteredCopy = isRegistered;
    isFollowedCopy = isFollowed;
    userIDCopy = userID;
    _nameLabel.text = [NSString stringWithFormat:@"%@ %@",firstName, lastName];
    _bioLabel.text = bio;
    //fetch picture;
    
    [self checkActionButtonState];
}

-(void) checkActionButtonState {
    if (isRegisteredCopy) {
        if (isFollowedCopy) {
            buttonState = unfollow;
            [_actionButton setTitle:@"Unfollow" forState:UIControlStateNormal];
            [_actionButton setBackgroundColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0]];
        } else {
            buttonState = follow;
            [_actionButton setTitle:@"Follow" forState:UIControlStateNormal];
            [_actionButton setBackgroundColor:[UIColor colorWithRed:36.0/255 green:105.0/255 blue:138.0/255 alpha:1.0]];
        }
    } else {
        buttonState = invite;
        [_actionButton setTitle:@"Invite" forState:UIControlStateNormal];
        [_actionButton setBackgroundColor:[UIColor colorWithRed:115.0/255 green:170.0/255 blue:100.0/255 alpha:1.0]];
    }
}



- (IBAction)onActionButtonPressed:(id)sender {
    if (isFollowedCopy) {
        [dataServiceAPI stopFollowingUser:userIDCopy withBlock:^(){
            isFollowedCopy = NO;
            [self checkActionButtonState];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"UpdatedFollowing"
             object:self];
        }];
    } else {
        [dataServiceAPI startFollowingUser:userIDCopy withBlock:^(){
            isFollowedCopy = YES;
            [self checkActionButtonState];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"UpdatedFollowing"
             object:self];
        }];
    }
}

@end
