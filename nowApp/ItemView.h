//
//  ItemView.h
//  nowApp
//
//  Created by Leonardo Amigoni on 2/5/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"

@interface ItemView : UIView

@property (weak, nonatomic) IBOutlet UIView *dotView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

- (void) configure: (Item *) itemData isNew: (BOOL)isNew;

@end
