//
//  ProfileInfo.h
//  nowApp
//
//  Created by Leonardo Amigoni on 2/10/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "JSONModel.h"

@interface ProfileInfo : JSONModel

@property (strong, nonatomic) NSString<Optional>* uid;
@property (strong, nonatomic) NSString<Optional>* firstName;
@property (strong, nonatomic) NSString<Optional>* lastName;
@property (strong, nonatomic) NSString<Optional>* bio;
@property (strong, nonatomic) NSString<Optional>* website;
@property (strong, nonatomic) NSString<Optional>* pictureURL;
@property (nonatomic) NSNumber* updateTime;
@property (nonatomic) NSNumber* createdTime;

@end
