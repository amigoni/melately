//
//  PersonSearch.m
//  nowApp
//
//  Created by Leonardo Amigoni on 2/10/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "UserSearch.h"

@implementation UserSearch

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    if ([propertyName isEqualToString: @"isFollowed"]) return YES;
    return NO;
}

@end
