//
//  IntroFinalViewController.m
//  nowApp
//
//  Created by Leonardo Amigoni on 2/2/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "IntroFinalViewController.h"
#import "UpdateStatusViewController.h"

@interface IntroFinalViewController ()

@end

@implementation IntroFinalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _getStartedButton.layer.cornerRadius = 10;
    _getStartedButton.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated{
    
}


- (IBAction)onGetStartedPressed:(id)sender {
    [self dismissViewControllerAnimated:NO completion: ^{
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"IntroClosed"
         object:self];
    }];
}


@end
