//
//  FindFriendsPersonTableViewCell.h
//  nowApp
//
//  Created by Leonardo Amigoni on 1/27/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Structs.h"

typedef enum {
    follow,
    unfollow,
    invite,
} ButtonState;

@interface FindFriendsPersonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *personIcon;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *bioLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

- (void) configure: (NSString *)firstName lastName:(NSString *)lastName bio:(NSString *)bio pictureURL:(NSString *)pictureURL isRegistered: (BOOL) isRegistered socialNetwork: (SocialNetwork) socialNetwork isFollowed: (BOOL) isFollowed userID: (NSString *)userID;

@end
