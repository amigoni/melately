//
//  ItemView.m
//  nowApp
//
//  Created by Leonardo Amigoni on 2/5/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "ItemView.h"


@implementation ItemView

- (void) configure: (Item *) itemData isNew:(BOOL)isNew {
    self.textLabel.text = itemData.text;
}

@end
