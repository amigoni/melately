//
//  EnterTextViewController.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "EnterTextViewController.h"
#import "DataServiceAPI.h"

@interface EnterTextViewController()

@property DataServiceAPI *dataServiceApi;

@end

@implementation EnterTextViewController

- (void)viewDidLoad {
    self.textView.delegate = self;
    self.textView.contentInset = UIEdgeInsetsMake(0,0,-10,-10);
    self.textView.textContainerInset = UIEdgeInsetsZero;
    [self.textView setText: self.text];
    if (self.text.length > 0) {
        self.textViewHintLabel.hidden = YES;
    }
    [self.textField setText: self.url];
    if (self.url.length > 0) {
        //self.textViewHintLabel.hidden = YES;
    }
    
    self.dataServiceApi = [DataServiceAPI sharedManager];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    long textLength = textView.text.length + (text.length - range.length);
    long charactersLeft = 140-textLength;
    self.charCounterLabel.text = [NSString stringWithFormat:@"%ld",charactersLeft];
    if (charactersLeft < 0) {
        self.charCounterLabel.textColor = [UIColor colorWithRed:201.0/255 green:77.0/255 blue:38.0/255 alpha:1.0];
    } else {
        self.charCounterLabel.textColor = [UIColor colorWithRed:153.0/255 green:153.0/255 blue:153.0/255 alpha:1.0];
    }
    
    if (textLength == 0) {
        self.textViewHintLabel.hidden = NO;
    } else {
        self.textViewHintLabel.hidden = YES;
    }
    
    return textLength <= 140;
}



- (BOOL)validateUrl: (NSString *)string {
    NSUInteger length = [string length];
    // Empty strings should return NO
    if (length > 0) {
        NSError *error = nil;
        NSDataDetector *dataDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
        if (dataDetector && !error) {
            NSRange range = NSMakeRange(0, length);
            NSRange notFoundRange = (NSRange){NSNotFound, 0};
            NSRange linkRange = [dataDetector rangeOfFirstMatchInString:string options:0 range:range];
            if (!NSEqualRanges(notFoundRange, linkRange) && NSEqualRanges(range, linkRange)) {
                return YES;
            }
        }
        else {
            NSLog(@"Could not create link data detector: %@ %@", [error localizedDescription], [error userInfo]);
        }
    }
    return NO;
}


# pragma mark - Buttons Actions

- (IBAction)onPostButtonPressed:(id)sender {
    
    BOOL urlTestResult = [self validateUrl:self.textField.text];
    
    if (urlTestResult) {
        self.url = self.textField.text;
    }
    
    [self.dataServiceApi saveItemData:self.position
                                 text:self.textView.text
                                  url:self.textField.text
                             position:self.position
                           completion:^{
                               self.completion();
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }
     ];
}


- (IBAction)onCloseButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
