//
//  Stucts.h
//  nowApp
//
//  Created by Leonardo Amigoni on 2/9/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Structs : NSObject

typedef enum {
    none,
    facebook,
    twitter,
    contacts,
} SocialNetwork;

@end
